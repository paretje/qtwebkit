#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

DEB_BUILD_ARCH     ?= $(shell dpkg-architecture -qDEB_BUILD_ARCH)
DEB_HOST_ARCH_BITS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_BITS)
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

CPPFLAGS = $(shell dpkg-buildflags --get CPPFLAGS) -Wall
CFLAGS = $(shell dpkg-buildflags --get CFLAGS)
LDFLAGS = $(shell dpkg-buildflags --get LDFLAGS) -Wl,--as-needed,--no-keep-memory

DEBHELPER_VERSION = $(shell dpkg-query -W -f='$${source:Version}' debhelper)

EXTRA_CMAKE_ARGUMENTS =

# Sacrifice speed in order to make it more likely resource limits
# won't be hit.
ifeq ($(DEB_HOST_ARCH_BITS),32)
	LDFLAGS += -Wl,--no-keep-memory
endif

ifeq ($(DEB_BUILD_ARCH),alpha)
	LDFLAGS += -Wl,--no-relax
endif

ifeq ($(DEB_BUILD_ARCH),arm64)
	EXTRA_CMAKE_ARGUMENTS += -DUSE_LD_GOLD=OFF
endif

# The debug packages produced by webkit are huge and cause problems in
# most buildds, so use -g1 in all architectures except the ones that
# are known to work fine
ifeq (,$(filter $(DEB_BUILD_ARCH),amd64 ppc64 ppc64el))
	CFLAGS := $(CFLAGS:-g=-g1)
endif

# See https://bugs.webkit.org/show_bug.cgi?id=113638
ifeq (,$(filter $(DEB_BUILD_ARCH),i386 amd64 hurd-i386 kfreebsd-i386 kfreebsd-amd64 armhf mipsel))
	EXTRA_CMAKE_ARGUMENTS += -DENABLE_JIT=OFF
	CPPFLAGS += -DENABLE_ASSEMBLER=0
endif

ifneq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
	EXTRA_CMAKE_ARGUMENTS += -DUSE_SYSTEM_MALLOC=ON
endif

ifneq (,$(filter debug,$(DEB_BUILD_OPTIONS)))
	EXTRA_CMAKE_ARGUMENTS += -DCMAKE_BUILD_TYPE=Debug
else
	EXTRA_CMAKE_ARGUMENTS += -DCMAKE_BUILD_TYPE=Release
	CPPFLAGS += -DNDEBUG -DG_DISABLE_CAST_CHECKS
endif

ifeq ($(shell dpkg-vendor --derives-from Ubuntu && echo yes),yes)
	DEB_DH_GENCONTROL_ARGS += -- -Vgst:Recommends=""
else
	DEB_DH_GENCONTROL_ARGS += -- -Vgst:Recommends="gstreamer1.0-plugins-bad, gstreamer1.0-libav,"
endif

%:
	dh $@

override_dh_auto_configure:
	CFLAGS="$(CFLAGS) $(CPPFLAGS)" \
	CXXFLAGS="$(CFLAGS) $(CPPFLAGS)" \
	LDFLAGS="$(LDFLAGS)" \
	dh_auto_configure -- \
	   -DPORT=Qt \
	   -DCMAKE_INSTALL_LIBEXECDIR=lib/$(DEB_HOST_MULTIARCH) \
	   -DCMAKE_C_FLAGS_RELEASE="" \
	   -DCMAKE_C_FLAGS_DEBUG="" \
	   -DCMAKE_CXX_FLAGS_RELEASE="" \
	   -DCMAKE_CXX_FLAGS_DEBUG="" \
	   $(EXTRA_CMAKE_ARGUMENTS)

override_dh_builddeb:
	DEB_BUILD_OPTIONS="$(filter-out parallel=%,$(DEB_BUILD_OPTIONS))" \
		dh_builddeb

override_dh_auto_test:
